package pl.spring.demo.mapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import pl.spring.demo.ntt.BookEntity;
import pl.spring.demo.to.BookTo;
import pl.spring.demo.to.AuthorTo;
@Component("mapper")
public class MapperToNtt {
	public MapperToNtt() {
		
	}
	public BookEntity bookToToEntity(BookTo book){
		return new BookEntity(book.getId(), authorListToString(book.getAuthors()), book.getTitle());
	}
	
	public BookTo bookEntityToTo (BookEntity book) {
		return new BookTo(book.getId(),book.getTitle(), stringToAuthorList(book.getAuthors()));
	}
	
	public List<BookTo> listBookEntityToTo (List <BookEntity> list) {
		List<BookTo> result = new ArrayList<BookTo>();
		for(BookEntity book: list) {
			result.add(bookEntityToTo(book));
		}
		return result;
	}
	
	public List<BookTo> setBookEntityToListTo (Set<BookEntity> bookSet) {
		List<BookTo> result = new ArrayList<BookTo>();
		for(BookEntity book: bookSet) {
			result.add(bookEntityToTo(book));
		}
		return result;
	}
	
	public List<BookEntity> listBookToToEntity (List <BookTo> list) {
		List<BookEntity> result = new ArrayList<BookEntity>();
		for(BookTo book: list) {
			result.add(bookToToEntity(book));
		}
		return result;
	}
	
	public List<AuthorTo> stringToAuthorList(String authors) {
		String[] authorsList = authors.split(";");
		List<AuthorTo> result = new ArrayList<AuthorTo>();
		for( int i=0;i<authorsList.length;i++)
		{
			String[] author = authorsList[i].split(" ");
			Long id=Long.parseLong(author[0]);
			String firstName=author[1];
			String lastName="";
			if(author.length>2)
				lastName=author[2];
			result.add(new AuthorTo(id, firstName, lastName));
		}
		return result;
	}
	
	private String authorListToString(List<AuthorTo> authorList) {
		String result="";
		for(AuthorTo author: authorList) {
			result+=author.getId()+" "+author.getName()+";";
		}
		return result;
	}
	
}
