package pl.spring.demo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pl.spring.demo.dao.BookDao;
import pl.spring.demo.mapper.MapperToNtt;
import pl.spring.demo.service.BookService;
import pl.spring.demo.to.BookTo;

import java.util.List;

@Service("bookService")
public class BookServiceImpl implements BookService {
    @Autowired
    private BookDao bookDao;
    @Autowired
    private MapperToNtt mapper;
    
    @Override
    @Cacheable("booksCache")
    public List<BookTo> findAllBooks() {
        return mapper.listBookEntityToTo(bookDao.findAll());
    }

    @Override
    public List<BookTo> findBooksByTitle(String title) {
        return mapper.listBookEntityToTo(bookDao.findBookByTitle(title));
    }

    @Override
    public List<BookTo> findBooksByAuthor(String author) {
        return mapper.listBookEntityToTo(bookDao.findBooksByAuthor(author));
    }

    @Override
    public BookTo saveBook(BookTo book) {
        return mapper.bookEntityToTo(bookDao.save(mapper.bookToToEntity(book)));
    }
    @Autowired
    public void setBookDao(BookDao bookDao) {
        this.bookDao = bookDao;
    }
}
