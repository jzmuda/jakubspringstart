package pl.spring.demo.dao.impl;

import pl.spring.demo.annotation.NullableId;
import pl.spring.demo.common.Sequence;
import pl.spring.demo.dao.BookDao;
import pl.spring.demo.mapper.MapperToNtt;
import pl.spring.demo.ntt.BookEntity;
import pl.spring.demo.to.AuthorTo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Repository("bookDao")
public class BookDaoImpl implements BookDao{
	
	@Autowired
	private MapperToNtt mapper;
    private final Set<BookEntity> ALL_BOOKS = new HashSet<>();

    private Sequence sequence;

    public BookDaoImpl() {
        addTestBooks();
    }
    @Override
    public List<BookEntity> findAll() {
        return new ArrayList<>(ALL_BOOKS);
    }
    @Override
    public List<BookEntity> findBookByTitle(String title) {
    	List<BookEntity> result = new ArrayList<BookEntity>();
    	for (BookEntity book: ALL_BOOKS ) {
    		String bookTitle = book.getTitle();
            if (title.equalsIgnoreCase(bookTitle)||hasPrefix(bookTitle.split(" "),title))
            	result.add(book);
        }
        return result;
    }
    
	@Override
    public List<BookEntity> findBooksByAuthor(String author) {
    	List<BookEntity> result = new ArrayList<BookEntity>();
    	for (BookEntity book : ALL_BOOKS ) {
            if (hasName(book,author))
            	result.add(book);
    	}
    	return result;
    }
    

    private boolean hasName(BookEntity book, String author) {
    	boolean result=false;
    	String[] name = author.split(" ");
    	String bookAuthors=book.getAuthors();
    	List<AuthorTo> authorsList=mapper.stringToAuthorList(bookAuthors);
    	if(name.length==2){
        	
        			result = authorEquals(authorsList, name);
    	}
    	else if (name.length==1) {	
    		String[] allNames = createStringList(authorsList);
    		result = hasPrefix(allNames, author);
    	}
    	else throw new IllegalArgumentException("Must provide name and surname or prefix");
		return result;
	}

    private boolean hasPrefix(String[] splitTitle, String title) {
		boolean result = false;
		title=title.toLowerCase();
		for(int i=0; i<splitTitle.length;i++) {
			if (splitTitle[i].toLowerCase().startsWith(title))
				result=true;
		}
		return result;
	}
 	
	private boolean authorEquals(List<AuthorTo> authorsList, String[] name) {
 		boolean result =false;
 		for(AuthorTo author1: authorsList){
    		if((author1.getFirstName().equalsIgnoreCase(name[0])&&author1.getLastName().equalsIgnoreCase(name[1]))||
    			(author1.getFirstName().equalsIgnoreCase(name[1])&&author1.getLastName().equalsIgnoreCase(name[0])))
    			result=true;
 		}
 		return result;
 	}

	private String[] createStringList(List<AuthorTo> authorsList) {
		ArrayList<String> nameList = new ArrayList<String>();
		for(AuthorTo author: authorsList) {
			nameList.add(author.getFirstName());
			nameList.add(author.getLastName());
		}
		return nameList.toArray(new String[nameList.size()]);
	}
    
    //private String[] splitAuthors()
    
    @Override
    @NullableId
    public BookEntity save(BookEntity book) {
    	/*if (book.getId() == null) {
    		book.setId(sequence.nextValue(ALL_BOOKS));
    	}*/
    	ALL_BOOKS.add(book);
    	return book;
    }

    
    @Autowired
    public void setSequence(Sequence sequence) {
        this.sequence = sequence;
    }
    @Override
    public Long getNextID() {
    	return sequence.nextValue(ALL_BOOKS);
    }
    

    private void addTestBooks() {
    	ALL_BOOKS.add(new BookEntity(1L, 1L+" Wiliam Szekspir;","Romeo i Julia"));
    	ALL_BOOKS.add(new BookEntity(2L, 2L+" Hanna Ożogowska;", "Opium w rosole"));
    	ALL_BOOKS.add(new BookEntity(3L, 3L+" Jan Paradowski;", "Przygody Odyseusza"));
    	ALL_BOOKS.add(new BookEntity(4L, 4L+" Edmund Niziurski;", "Awantura w Niekłaju"));
    	ALL_BOOKS.add(new BookEntity(5L, 5L+" Zbigniew Nienacki;", "Pan Samochodzik i Fantomas"));
    	ALL_BOOKS.add(new BookEntity(6L, 6L+" Aleksander Fredro;", "Zemsta"));
    }
}
