package pl.spring.demo.aop;


import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.MethodBeforeAdvice;
import org.springframework.stereotype.Component;

import pl.spring.demo.annotation.NullableId;
import pl.spring.demo.dao.BookDao;
import pl.spring.demo.exception.BookNotNullIdException;
import pl.spring.demo.exception.IllegalDaoArgumentException;
import pl.spring.demo.to.IdAware;

import java.lang.reflect.Method;

@Component
@Aspect
public class BookDaoAdvisor implements MethodBeforeAdvice {
	
    @Override
    public void before(Method method, Object[] arguments, Object target) throws Throwable {

        if (hasAnnotation(method, target, NullableId.class)) {
        	Object argument=arguments[0];
            checkNotNullId(target, argument);
        }
    }

    private void checkNotNullId(Object target, Object argument) {
        if (argument instanceof IdAware && ((IdAware) argument).getId() != null) {
            throw new BookNotNullIdException();
        }
        else if (argument instanceof IdAware)
        	setIdToAwareObject(target,argument);
        else throw new IllegalDaoArgumentException();
    }
    
    private void setIdToAwareObject(Object target, Object argument){
    	((IdAware) argument).setId(((BookDao) target).getNextID());
    }

    private boolean hasAnnotation (Method method, Object o, Class annotationClazz) throws NoSuchMethodException {
        boolean hasAnnotation = method.getAnnotation(annotationClazz) != null;

        if (!hasAnnotation && o != null) {
            hasAnnotation = o.getClass().getMethod(method.getName(), method.getParameterTypes()).getAnnotation(annotationClazz) != null;
        }
        return hasAnnotation;
    }
}