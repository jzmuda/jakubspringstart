package pl.spring.demo.mock;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.spring.demo.dao.BookDao;
import pl.spring.demo.mapper.MapperToNtt;
import pl.spring.demo.ntt.BookEntity;
import pl.spring.demo.service.impl.BookServiceImpl;
import pl.spring.demo.to.AuthorTo;
import pl.spring.demo.to.BookTo;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

public class BookServiceImplTest {
	@Mock
	private MapperToNtt mapper;
    @InjectMocks
    private BookServiceImpl bookService;
    @Mock
    private BookDao bookDao;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testShouldSaveBook() {
    	// given
    	String fooAuthor=("1 Name Surname;");
    	BookEntity book = new BookEntity(null, fooAuthor, "title");
    	BookTo bookAsTo = new BookTo(null,"title",Arrays.asList(new AuthorTo(1L,"Name","Surname")));
    	Mockito.when(bookDao.save(book)).thenReturn(new BookEntity(1L, fooAuthor, "title"));
        // when
        BookTo result = bookService.saveBook(mapper.bookEntityToTo(book));
        // then
        System.out.println(book.getAuthors());
        Mockito.verify(bookDao).save(Mockito.any(BookEntity.class));
        assertEquals(1L, result.getId().longValue());
    }
}
