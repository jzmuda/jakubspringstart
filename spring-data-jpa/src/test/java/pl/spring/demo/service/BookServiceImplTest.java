package pl.spring.demo.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.spring.demo.exception.BookNotNullIdException;
import pl.spring.demo.to.AuthorTo;
import pl.spring.demo.to.BookTo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "CommonServiceTest-context.xml")
public class BookServiceImplTest {

	@Autowired
    private BookService bookService;
    @Autowired
    private CacheManager cacheManager;

    @Before
    public void setUp() {
        cacheManager.getCache("booksCache").clear();
    }

    @Test
    public void testShouldFindAllBooks() {
        // when
        List<BookTo> allBooks = bookService.findAllBooks();
        // then
        assertNotNull(allBooks);
        assertFalse(allBooks.isEmpty());
        boolean greaterEqualThan=allBooks.size()>=6;
        assertEquals(true, greaterEqualThan);
    }
    
    @Test
    public void testShouldFindAllBooksByTitle() {
        // given
        final String title = "Opium w rosole";
        // when
        List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
        // then
        assertNotNull(booksByTitle);
        assertFalse(booksByTitle.isEmpty());
    }
    
    @Test
    public void testShouldFindAllBooksByTitleIgnoreCase() {
    	// given
    	final String title = "oPium w roSOle";
    	// when
    	List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
    	// then
    	assertNotNull(booksByTitle);
    	assertFalse(booksByTitle.isEmpty());
    }
    
    @Test
    public void testShouldFindAllBooksByPrefix() {
    	// given
    	final String title = "przy";
    	// when
    	List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
    	// then
    	assertNotNull(booksByTitle);
    	assertEquals(1,booksByTitle.size());
    }
    
    @Test
    public void testShouldNotFindBooksByNonexistentTitle() {
    	// given
    	final String title = "Opium w szkole";
    	// when
    	List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
    	// then
    	assertNotNull(booksByTitle);
    	assertTrue(booksByTitle.isEmpty());
    }
    
    @Test
    public void testShouldFindAllBooksByAuthor() {
    	// given
    	final String author = "Hanna Ożogowska";
    	// when
    	List<BookTo> booksByAuthor = bookService.findBooksByAuthor(author);
    	// then
    	assertNotNull(booksByAuthor);
    	assertFalse(booksByAuthor.isEmpty());
    }
    
    @Test
    public void testShouldFindAllBooksByAuthorIgnoreCase() {
    	// given
    	final String author = "HaNNa ożogowska";
    	// when
    	List<BookTo> booksByAuthor = bookService.findBooksByAuthor(author);
    	// then
    	assertNotNull(booksByAuthor);
    	assertFalse(booksByAuthor.isEmpty());
    }
    
    @Test
    public void testShouldFindTwoShakespeareBooks() {
    	// given
    	BookTo hamlet=new BookTo();
    	hamlet.setAuthors(Arrays.asList(new AuthorTo(1L,"Wiliam","Szekspir")));
    	hamlet.setTitle("Hamlet");
    	bookService.saveBook(hamlet);
    	final String author = "WILIAM SZEKSPIR";
    	// when
    	List<BookTo> booksByAuthor = bookService.findBooksByAuthor(author);
    	// then
    	assertNotNull(booksByAuthor);
    	assertEquals(2,booksByAuthor.size());
    }
    
    @Test
    public void testShouldFindTwoRomeoAndJuliet() {
    	// given
    	BookTo rij=new BookTo();
    	rij.setAuthors(Arrays.asList(new AuthorTo(10l, "Bezczelny", "Plagiator")));
    	final String title = "Romeo i Julia";
    	rij.setTitle(title);
    	bookService.saveBook(rij);
    	// when
    	List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
    	// then
    	assertNotNull(booksByTitle);
    	assertEquals(2,booksByTitle.size()); 
    	
    }
    
    @Test
    public void testShouldFindBookTitlePrefix() {
    	// given
    	String title = "zEm";
    	// when
    	List<BookTo> booksByTitle = bookService.findBooksByTitle(title);
    	// then
    	assertNotNull(booksByTitle);
    	assertEquals(1,booksByTitle.size());
    }
    
    @Test
    public void testShouldFindBooksAuthorPrefix() {
    	// given
    	final String author = "Han";
    	List<BookTo> booksByAuthor= new ArrayList<BookTo>();
    	// when
    	booksByAuthor = bookService.findBooksByAuthor(author);
    	// then
    	assertNotNull(booksByAuthor);
    	assertEquals(1,booksByAuthor.size());
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void testShouldThrowExceptionLongName() {
    	// given
    	final String author = "a b c";
    	// when
    	List<BookTo> booksByAuthor = bookService.findBooksByAuthor(author);
    	// then
    	fail("test should throw IllegalArgumentException");
    }

    @Test(expected = BookNotNullIdException.class)
    public void testShouldThrowBookNotNullIdException() {
        // given
       	final BookTo bookToSave = new BookTo(22L,"Pan Tadeusz",Arrays.asList(new AuthorTo(1L,"Name","Surname")));
        // when
        bookService.saveBook(bookToSave);
        // then
        fail("test should throw BookNotNullIdException");
    }
}
